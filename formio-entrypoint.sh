#!/bin/sh

# Set configuration defaults based on environment variables.
cat > /app/config/default.json <<- EOM
{
  "port": 3001,
  "appPort": 8080,
  "host": "${HOST}",
  "protocol": "${PROTOCOL}",
  "allowedOrigins": ["*"],
  "domain": "${DOMAIN}",
  "basePath": "${BASE_PATH}",
  "mongo": "mongodb://${MONGO_USERNAME}:${MONGO_PASSWORD}@${MONGO_HOST}:${MONGO_PORT}/${MONGO_DATABASE}${MONGO_OPTIONS}",
  "mongoConfig": "",
  "mongoSA": "",
  "mongoSecret": "${MONGO_SECRET}",
  "reservedForms": [
    "submission",
    "exists",
    "export",
    "role",
    "current",
    "logout",
    "import",
    "form",
    "access",
    "token",
    "recaptcha"
  ],
  "jwt": {
    "secret": "${JWT_SECRET}",
    "expireTime": 240
  },
  "email": {
    "type": "smtp"
  },
  "settings": {
    "email": {
      "smtp": {
        "allowUnauthorizedCerts": "false",
        "host": "${SMTP_HOST}",
        "port": "${SMTP_PORT}",
        "secure": "${SMTP_SECURE}",
        "auth": {
          "user": "${SMTP_USERNAME}",
          "pass": "${SMTP_PASSWORD}"
        }
      }
    }
  }
}
EOM

# Overwrite the template config file.
cat > /app/client/dist/config.js <<- EOM
var APP_URL = "${DOMAIN}";
var API_URL = "${DOMAIN}";

// Parse query string
var query = {};
location.search.substr(1).split("&").forEach(function(item) {
  query[item.split("=")[0]] = item.split("=")[1] && decodeURIComponent(item.split("=")[1]);
});

var appUrl = query.appUrl || APP_URL;
var apiUrl = query.apiUrl || API_URL;

angular.module('formioApp').constant('AppConfig', {
  appUrl: appUrl,
  apiUrl: apiUrl,
  forms: {
    userForm: appUrl + '/user',
    userLoginForm: appUrl + '/user/login'
  }
});
EOM

# Unset environment variables used only in the configuration file.
unset BASE_PATH
unset HOST
unset DOMAIN
unset JWT_SECRET
unset MONGO_DATABASE
unset MONGO_HOST
unset MONGO_PASSWORD
unset MONGO_PORT
unset MONGO_SECRET
unset MONGO_USERNAME
unset MONGO_OPTIONS
unset PROTOCOL
unset SMTP_HOST
unset SMTP_PASSWORD
unset SMTP_PORT
unset SMTP_SECURE
unset SMTP_USERNAME

# Execute command.
exec "$@"
