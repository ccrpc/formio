FROM node:20-alpine3.19
WORKDIR /app
ENV FORMIO_VERSION 4.2.8
ENV CLIENT_VERSION 1.5.0

RUN wget -qO- https://github.com/formio/formio/archive/v${FORMIO_VERSION}.tar.gz | \
  tar -xvz --strip-components=1 -C /app \
  formio-${FORMIO_VERSION}

RUN mkdir -p /app/client && \
  wget -qO- https://github.com/formio/formio-app-formio/archive/${CLIENT_VERSION}.tar.gz | \
  tar -xvz --strip-components=1 -C /app/client \
  formio-app-formio-${CLIENT_VERSION}

# Install necessary build tools and dependencies
RUN apk add --no-cache --virtual .build-deps \
  make \
  python3 \
  g++ \
  git

# Use https to avoid requiring ssh keys for public repos.
RUN git config --global url."https://github.com/".insteadOf "ssh://git@github.com/"

# Install dependencies using yarn
RUN yarn install

# Remove git to reduce image size
RUN apk del git

# Remove apk del --no-cache .build-deps
RUN apk del --no-cache .build-deps

COPY formio-entrypoint.sh /app

ENV BASE_PATH="/"
ENV HOST="localhost:3001"
ENV DEBUG=""
ENV DOMAIN="http://localhost:3001"
ENV JWT_SECRET=""
ENV MONGO_DATABASE="formio"
ENV MONGO_HOST="mongo"
ENV MONGO_PASSWORD=""
ENV MONGO_PORT="27017"
ENV MONGO_SECRET=""
ENV MONGO_USERNAME=""
ENV MONGO_OPTIONS=""
ENV PROTOCOL="http"
ENV ROOT_EMAIL="admin@example.com"
ENV ROOT_PASSWORD="password"
ENV SMTP_HOST=""
ENV SMTP_PASSWORD=""
ENV SMTP_PORT="587"
ENV SMTP_SECURE="false"
ENV SMTP_USERNAME=""

EXPOSE 3001

ENTRYPOINT ["/app/formio-entrypoint.sh"]
CMD [ "node", "--no-node-snapshot", "main" ]