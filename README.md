# Form.io Docker Image
Docker image for the open source form management platform
[Form.io](https://github.com/formio/formio).

## License
Form.io Docker Image is available under the terms of the [BSD 3-clause
license](https://gitlab.com/ccrpc/formio/blob/master/LICENSE.md).
Form.io is licensed under the
[OSL-v3 license](https://opensource.org/licenses/OSL-3.0).
