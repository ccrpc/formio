# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## 2.0.0 (2021-03-15)


### Features

* upgrade Form.io to 2.0 ([e3d9710](https://gitlab.com/ccrpc/formio/commit/e3d9710fb44c5131fb0ce20c4fc84adb8116e87b))


### Bug Fixes

* preinstall client to avoid duplicate admins ([0cc6867](https://gitlab.com/ccrpc/formio/commit/0cc6867d40007f7a2479e2aa2a607e20bdc8d010))
